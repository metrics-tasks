README: Perform an ad-hoc analysis of Tor's sanitized web logs
==============================================================

Sanitized versions of Tor's Apache web logs are available at
https://webstats.torproject.org/.  Let's perform an ad-hoc analysis of
these logs to decide what graphs we'll want to put on Tor Metrics.  And
let's perform this analysis by throwing everything into a PostgreSQL
database that we might later want to re-use for Tor Metrics.  

Steps to import sanitized web logs into the database:

Create PostgreSQL database user webstats and database of same name:

$ sudo -u postgres createuser -P webstats
$ sudo -u postgres createdb -O webstats webstats

Import database schema:

$ psql -f webstats.sql webstats

Fetch sanitized web logs and put them under webstats.torproject.org/:

$ wget --recursive --reject "index.html*" --no-parent \
  --accept "*201608*" https://webstats.torproject.org/

Fetch the following required libraries and put them in the lib/
folder:

 - lib/commons-compress-1.9.jar
 - lib/postgresql-jdbc3-9.2.jar
 - lib/xz-1.5.jar

Run the importer:

$ ./run.sh

Log into database and run a simple query:

$ psql webstats

webstats=> SELECT log_date, SUM(count) AS hits FROM requests
  NATURAL JOIN resources NATURAL JOIN files
  WHERE method = 'GET' AND response_code = 200
  AND (resource_string = '/' OR resource_string LIKE '/index%')
  AND site = 'www.torproject.org' GROUP BY log_date ORDER BY log_date;

