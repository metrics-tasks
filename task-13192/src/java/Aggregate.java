import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.TreeMap;

public class Aggregate {

  private static File hidservStatsCsvFile =
      new File("out/csv/hidserv-stats.csv");

  private static File hidservStatsExtrapolatedCsvFile =
      new File("out/csv/hidserv-stats-extrapolated.csv");

  public static void main(String[] args) throws Exception {
    aggregate();
  }

  private static final DateFormat DATE_TIME_FORMAT, DATE_FORMAT;

  static {
    DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DATE_TIME_FORMAT.setLenient(false);
    DATE_TIME_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
    DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    DATE_FORMAT.setLenient(false);
    DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
  }

  private static void aggregate() throws Exception {
    if (!hidservStatsCsvFile.exists() ||
        hidservStatsCsvFile.isDirectory()) {
      System.err.println("Unable to read "
          + hidservStatsCsvFile.getAbsolutePath() + ".  Exiting.");
      System.exit(1);
    }
    SortedMap<String, List<double[]>>
        extrapolatedCells = new TreeMap<String, List<double[]>>(),
        extrapolatedOnions = new TreeMap<String, List<double[]>>();
    BufferedReader br = new BufferedReader(new FileReader(
        hidservStatsCsvFile));
    String line = br.readLine();
    while ((line = br.readLine()) != null) {
      String[] parts = line.split(",");
      String date = DATE_FORMAT.format(DATE_TIME_FORMAT.parse(parts[2]));
      double hidservRendRelayedCells = Double.parseDouble(parts[3]),
          hidservDirOnionsSeen = Double.parseDouble(parts[4]),
          fracRendRelayedCells = Double.parseDouble(parts[5]),
          fracDirOnionsSeen = Double.parseDouble(parts[6]);
      
      if (fracRendRelayedCells > 0.0) {
        if (!extrapolatedCells.containsKey(date)) {
          extrapolatedCells.put(date, new ArrayList<double[]>());
        }
        extrapolatedCells.get(date).add(new double[] {
            hidservRendRelayedCells * 512.0 * 8.0
            / (86400.0 * 1000000.0 * fracRendRelayedCells),
            fracRendRelayedCells });
      }
      if (fracDirOnionsSeen > 0.0) {
        if (!extrapolatedOnions.containsKey(date)) {
          extrapolatedOnions.put(date, new ArrayList<double[]>());
        }
        extrapolatedOnions.get(date).add(new double[] {
            hidservDirOnionsSeen / (12.0 * fracDirOnionsSeen),
            fracDirOnionsSeen });
      }
    }
    br.close();
    hidservStatsExtrapolatedCsvFile.getParentFile().mkdirs();
    BufferedWriter bw = new BufferedWriter(new FileWriter(
        hidservStatsExtrapolatedCsvFile));
    bw.write("date,type,wmean,wmedian,wiqm\n");
    for (int i = 0; i < 2; i++) {
      String type = i == 0 ? "cells" : "onions";
      SortedMap<String, List<double[]>> extrapolated = i == 0
          ? extrapolatedCells : extrapolatedOnions;
      for (Map.Entry<String, List<double[]>> e :
        extrapolated.entrySet()) {
        String date = e.getKey();
        List<double[]> weightedValues = e.getValue();
        double totalFrac = 0.0;
        for (double[] d : weightedValues) {
          totalFrac += d[1];
        }
        if (totalFrac < 0.01) {
          continue;
        }
        Collections.sort(weightedValues,
            new Comparator<double[]>() {
          public int compare(double[] o1, double[] o2) {
            return o1[0] < o2[0] ? -1 : o1[0] > o2[0] ? 1 : 0;
          }
        });
        double totalWeighted = 0.0, totalProbability = 0.0;
        double totalInterquartileFrac = 0.0,
            totalWeightedInterquartile = 0.0;
        Double weightedMedian = null;
        for (double[] d : weightedValues) {
          totalWeighted += d[0] * d[1];
          totalProbability += d[1];
          if (weightedMedian == null &&
              totalProbability > totalFrac * 0.5) {
            weightedMedian = d[0];
          }
          if (totalProbability >= totalFrac * 0.25 &&
              totalProbability - d[1] <= totalFrac * 0.75) {
            totalWeightedInterquartile += d[0] * d[1];
            totalInterquartileFrac += d[1];
          }
        }
        bw.write(String.format("%s,%s,%.0f,%.0f,%.0f%n", date, type,
            totalWeighted / totalProbability, weightedMedian,
            totalWeightedInterquartile / totalInterquartileFrac));
      }
    }
    bw.close();
  }
}
